from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import time
from pytz import timezone
from datetime import datetime
import dateutil.parser
from pandas.tseries.holiday import AbstractHolidayCalendar, Holiday, nearest_workday, \
    USMartinLutherKingJr, USPresidentsDay, GoodFriday, USMemorialDay, \
    USLaborDay, USThanksgivingDay


class USTradingCalendar(AbstractHolidayCalendar):
    rules = [
        Holiday('NewYearsDay', month=1, day=1, observance=nearest_workday),
        USMartinLutherKingJr,
        USPresidentsDay,
        GoodFriday,
        USMemorialDay,
        Holiday('USIndependenceDay', month=7, day=4, observance=nearest_workday),
        USLaborDay,
        USThanksgivingDay,
        Holiday('Christmas', month=12, day=25, observance=nearest_workday),
        Holiday('BushSeniorDeath', year=2018, month=12, day=5)
        # added BushSeniors Death Mourning holiday on 5th December 2018, see Mail from December 3, 2018
    ]

def is_trading_day(date):
    inst = USTradingCalendar()
    holidays = inst.holidays(datetime(date.year-1, 12, 31), datetime(date.year, 12, 31))

    return date not in holidays and date.weekday() < 5

#scrap old tweets based on username, so the list of user names needed, which can be collected with stocktwit.py
#the current function is for the old posts when the webpage is unable to laod the historical posts(e.g., already scroll 1000 page and still can not reach the date needed, and the webpage is crashed)
#the current function nedd to buy for StockTwit premium membership
#inputs:
    #dt, string:year and month, e.g. "2019-08'
    #uid,string: user name list, should collect with stocktwit.py first
    #com,string: stock company ticker, e.g., "BA"
    #userdict, dictionary: collected by stocktwit.py
    
def get_oldtwits(dt,uid,com,userdict):
    browser=webdriver.Firefox(executable_path=r'path/geckodriver.exe')
    browser.get("https://stocktwits.com")
    browser.find_elements_by_xpath("//*[contains(text(), 'Accept Cookies')]")[1].click()
    time.sleep(2)
    browser.find_elements_by_xpath("//*[contains(text(), 'Log In')]")[0].click()
    time.sleep(2)
    browser.find_elements_by_xpath("//input[@type='text'][@name='login']")[0].send_keys("gXXXX")
    time.sleep(2)
    browser.find_elements_by_xpath("//input[@type='password'][@name='password']")[0].send_keys("XXXX")
    time.sleep(2)
    browser.find_elements_by_xpath("//button[@type='submit'][contains(text(), 'Log In')]")[0].click()
    time.sleep(2)
    i=0
    while i<len(uid):
            user = uid[i]    
            try:
                browser.get("https://stocktwits.com/search/u{}?period={}&symbol=".format(user,dt)+com)
                time.sleep(3)
                soup = BeautifulSoup( browser.page_source,"lxml")
                tweets = soup.findAll('article',attrs={'class':'st_VgdfbpJ'})
                if len(tweets)>0:
                    last_height = browser.execute_script("return document.body.scrollHeight")
                    y=3000
                        
                    while True:
                        # Scroll down to bottom
                        browser.execute_script("window.scrollTo(0, "+str(y)+")")
                        time.sleep(1)
                        try:
                            soup = BeautifulSoup( browser.page_source,"lxml")
                            tweets.extend(soup.body.findAll('article',attrs={'class':'st_VgdfbpJ'}))
                            
                            y += 2400
                        except:
                            y += 2400
                        # Calculate new scroll height and compare with last scroll height
                        new_height = browser.execute_script("return document.body.scrollHeight")
                        if new_height == last_height:
                            browser.execute_script("window.scrollTo(0, "+str(y)+")")
                            time.sleep(2)
                            try:
                                soup = BeautifulSoup( browser.page_source,"lxml")
                                tweets.extend(soup.body.findAll('article',attrs={'class':'st_VgdfbpJ'}))
                                y += 2400
                            except:
                                y += 2400
                                
                            new_height = browser.execute_script("return document.body.scrollHeight")
                            if new_height == last_height:
                                break
                        last_height = new_height
                    i+=1
                    break
                else:
                    i+=1
            except:
                i+=1
        
    for j in range(i,len(uid)):
        user=uid[j]
        try:
            lastl=len(tweets)
            browser.get("https://stocktwits.com/search/u{}?period={}&symbol=".format(user,dt)+com)
            time.sleep(3)
            soup = BeautifulSoup( browser.page_source,"lxml")
            tweets.extend(soup.findAll('article',attrs={'class':'st_VgdfbpJ'}))
            
            if len(tweets)>lastl:
                last_height = browser.execute_script("return document.body.scrollHeight")
                y=3000
                                
                while True:
                    # Scroll down to bottom
                    browser.execute_script("window.scrollTo(0, "+str(y)+")")
                    time.sleep(1)
                    try:
                        soup = BeautifulSoup( browser.page_source,"lxml")
                        tweets.extend(soup.body.findAll('article',attrs={'class':'st_VgdfbpJ'}))
                        y += 2400
                    except:
                        y += 2400
                    # Calculate new scroll height and compare with last scroll height
                    new_height = browser.execute_script("return document.body.scrollHeight")
                    if new_height == last_height:
                        browser.execute_script("window.scrollTo(0, "+str(y)+")")
                        time.sleep(2)
                        try:
                            soup = BeautifulSoup( browser.page_source,"lxml")
                            tweets.extend(soup.body.findAll('article',attrs={'class':'st_VgdfbpJ'}))

                            y += 2400
                        except:
                            y += 2400
                            
                        new_height = browser.execute_script("return document.body.scrollHeight")
                        if new_height == last_height:
                            break
                    last_height = new_height

        except:
            print(j)

        
    sentiments = []
    dates = []
    year=[]
    month =[]
    day=[]
    hour=[]
    minute=[]
    is_trade=[]
    twits = []
    likes = []
    user_ids = []
    followers = []
    company = []
    for a in tweets:
        #Get tweet posting time
        try:
            date = a.find('a', href = True, attrs={'class':'st_28bQfzV st_1E79qOs st_3TuKxmZ st_3Y6ESwY st_GnnuqFp st_1VMMH6S'}).text
        except:
            date = None
        
        if date is not None and dateutil.parser.parse(date) > datetime.strptime('2018-08-01', '%Y-%m-%d'): #and datetime.strptime(date.replace('th,', ' 2019').replace(' pm','PM').replace(' am', 'AM'), '%b %d %Y %I:%M%p')< datetime.strptime(date_until, '%b %d %Y %I:%M%p'):
            try:
                ts = dateutil.parser.parse(date)
                ts=ts.replace(tzinfo=timezone('Europe/Berlin'))
                ts=ts.astimezone(timezone('US/Eastern'))   
                dates.append(str(ts))
                year.append(ts.year)
                month.append(ts.month)
                day.append(ts.day)
                hour.append(ts.hour)
                minute.append(ts.minute)
                is_trade.append(is_trading_day(ts))                
            except:
                dates.append(None)
            #Company
            company.append(com)   
            #Get sentiment tags
            try:
                senti = a.find('div', attrs={'class':'lib_XwnOHoV lib_3UzYkI9 lib_lPsmyQd lib_2TK8fEo'})
                sentiments.append(senti.text)
            except:
                sentiments.append(None)
                
            #Get tweet content        
            
            try:
                twit = a.find('div', attrs={'class':'st_3SL2gug'})
                twits.append(twit.get_text(strip = True))
            except:
                twits.append(None)
            
            #Get like counts        
            try:
                like = a.find('span', attrs={'class':'st_1tZ744c st_VNaOUo1 st_39Uuz7b st_VNaOUo1 st_3OfMfdC st_3hA0I6Z'})
                likes.append(like.text)
            except:
                likes.append(None)
            
            #user related
            user = a.find('a', href = True, attrs={'class':'st_x9n-9YN st_VNaOUo1 st_2LcBLI2 st_1vC-yaI st_1VMMH6S'}).attrs['href']
            user_ids.append(user)
            if user in userdict:
                followers.append(userdict.get(user))
            else:
                try:
                    browser.get("https://stocktwits.com"+user)
                    time.sleep(2)
                    user_page = BeautifulSoup(browser.page_source,'lxml')
                    follower = user_page.findAll('h2', attrs ={'class':"st__tZJhLh st_29vRgrA st_2LcBLI2 st_2h5TkHM st_8u0ePN3 st_2mehCkH st_CjvTpBY st_2z6yeWY"})[2]
                    followers.append(follower.text)
                    userdict[user] = follower.text
                except:
                    followers.append('0')

        
    
    d = {'company':company,
         'date': dates,
         'day':day,
         'followers': followers,
         'hour':hour,
         'is_trade':is_trade,
         'likes': likes,
         'minute':minute,
         'month':month,
         'sentiments':sentiments,
         'tweets': twits,
         'user_ids': user_ids,        
         'year':year}
    
    df = pd.DataFrame(data = d)
    df=df.drop_duplicates(keep='first')
    
    df.to_csv (r'path.csv', index = None, header= False, mode = 'a')
    browser.close()
    return len(df)


months=['2019-04','2019-05','2019-06','2019-07']
td=pd.read_csv(r'path.csv',sep=',', error_bad_lines=False, index_col=False, dtype='unicode') 
tmp = td.drop_duplicates(subset='user_ids', keep='first')
tmp=tmp.loc[tmp.followers != None]
users = tmp["user_ids"].values
followers = tmp["followers"].values
zipbObj = zip(users, followers) 
# Create a dictionary from zip object
userdict = dict(zipbObj)

uid=list(set(users))

from joblib import Parallel, delayed
results = Parallel(n_jobs=4)(delayed(get_oldtwits)(s,uid,'BA',userdict) for s in months)
