from sklearn import model_selection, preprocessing, linear_model, naive_bayes, metrics, svm
from sklearn.feature_extraction.text import TfidfVectorizer #, CountVectorizer
import pandas as pd
from nltk.util import ngrams
import numpy as np
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
analyser = SentimentIntensityAnalyzer()
#from sentimentcorpus import sent_corp
from preprocess import preprocess

#load scrapped data
tdd=pd.read_csv(r'path.csv',sep=',', error_bad_lines=False, index_col=False, dtype='unicode') 
tdd=tdd.drop_duplicates(keep='last')
snt= tdd[["user_ids","sentiments","tweets"]]
snt=snt.loc[snt["sentiments"].isin(['Bearish','Bullish'])]

#preprocessing
snt['tweets3']=preprocess(list(snt['tweets']),'clean')

#count preprocessed textual information word counts to exclude the ones with too limited information
wcnt=[]
for i in range(len(snt)):
    t=snt.iloc[i].tweets3
    l=t.split(' ')
    l=[w for w in l if w != '']
    wcnt.append(len(l))

snt['wordcnt']=wcnt
snt3 = snt.drop_duplicates(subset='tweets3', keep='last') #deduplicated the training corpus by preprocessed post content
snt3=snt.loc[snt.wordcnt>=3] #exclude the posts containing less than 3 words
print(snt.groupby("sentiments").count())
print(snt3.groupby("sentiments").count())


#create balanced corpus
bull=snt3.loc[snt3.sentiments=='Bullish']
bear=snt3.loc[snt3.sentiments=='Bearish']
bull_s = bull.sample( n=len(bear),random_state=123)
snt_b=bull_s.append(bear)

print(snt_b.groupby("sentiments").count())

# split the dataset into training and validation datasets 
train_text, test_text, train_label, test_label = model_selection.train_test_split(snt_b['tweets3'], snt_b['sentiments'],test_size=0.2, random_state=123)

# normalize the label to numerical variables 
encoder = preprocessing.LabelEncoder()
train_label = encoder.fit_transform(train_label)
test_label = encoder.fit_transform(test_label)


# create a unigram count vectorizer object 
#count_vect = CountVectorizer(analyzer='word', token_pattern=r'\w{1,}',min_df=0.001,max_df=0.999)
#count_vect.fit(snt_b['tweets3'])
# transform the training and validation data using count vectorizer object
#train_text_cnt =  count_vect.transform(train_text)
#test_text_cnt =  count_vect.transform(test_text)

# create a bigram count vectorizer object 
#count_vect_bigram = CountVectorizer(analyzer='word', token_pattern=r'\w{1,}',min_df=0.001,ngram_range=(2,2),max_df=0.999)
#count_vect_bigram.fit(snt_b['tweets3'])
# transform the training and validation data using count vectorizer object
#train_text_cnt_bigram =  count_vect_bigram.transform(train_text)
#test_text_cnt_bigram =  count_vect_bigram.transform(test_text)


# word level tf-idf
tfidf_vect = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', min_df=0.0001,max_df=0.90)
tfidf_vect.fit(snt_b['tweets3'])
train_text_tfidf =  tfidf_vect.transform(train_text)
test_text_tfidf =  tfidf_vect.transform(test_text)

# ngram level tf-idf 
tfidf_vect_bigram = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', ngram_range=(1,2), min_df=0.0001,max_df=0.9)
tfidf_vect_bigram.fit(snt_b['tweets3'])
train_text_tfidf_bigram =  tfidf_vect_bigram.transform(train_text)
test_text_tfidf_bigram =  tfidf_vect_bigram.transform(test_text)


def train_model(classifier, feature_vector_train, label, feature_vector_valid,test):
    # fit the training dataset on the classifier
    classifier.fit(feature_vector_train, label)
    
    # predict the labels on validation dataset
    predictions = classifier.predict(feature_vector_valid)
    
    return metrics.accuracy_score(predictions, test)

def apply_model(classifier, feature_vector_train, label, feature_vector_unknown,mode):
    # fit the training dataset on the classifier
    classifier.fit(feature_vector_train, label)
    
    # predict the labels on validation dataset
    predictions1 = classifier.predict_proba(feature_vector_unknown)
    predictions2 = classifier.predict(feature_vector_unknown)
    if mode == 'pro':
        return predictions1        
    elif mode == 'logit':
        return predictions2
    else:
        return predictions1,predictions2


# Naive Bayes 
#accuracy = train_model(naive_bayes.MultinomialNB(), train_text_cnt, train_label, test_text_cnt,test_label)
#print ("NB, Count Vectors: ", accuracy)
# Naive Bayes on Count bigram Vectors
#accuracy = train_model(naive_bayes.MultinomialNB(), train_text_cnt_bigram, train_label, test_text_cnt_bigram,test_label)
#print ("NB, Bi-Gram Count: ", accuracy)
# Naive Bayes on Word Level TF IDF Vectors
accuracy = train_model(naive_bayes.MultinomialNB(), train_text_tfidf, train_label, test_text_tfidf,test_label)
print ("NB, WordLevel TF-IDF: ", accuracy)
# Naive Bayes on bigram Level TF IDF Vectors
accuracy = train_model(naive_bayes.MultinomialNB(), train_text_tfidf_bigram, train_label, test_text_tfidf_bigram,test_label)
print ("NB, Bi-Gram TF-IFD: ", accuracy)


# Logistic regression
#accuracy = train_model(linear_model.LogisticRegression(), train_text_cnt, train_label, test_text_cnt,test_label)
#print ("MaxEnt, Count Vectors: ", accuracy)
#accuracy = train_model(linear_model.LogisticRegression(), train_text_cnt_bigram, train_label, test_text_cnt_bigram,test_label)
#print ("MaxEnt, Bi-Gram Count: ", accuracy)
accuracy = train_model(linear_model.LogisticRegression(), train_text_tfidf, train_label, test_text_tfidf,test_label)
print ("MaxEnt, WordLevel TF-IDF: ", accuracy)
accuracy = train_model(linear_model.LogisticRegression(), train_text_tfidf_bigram, train_label, test_text_tfidf_bigram,test_label)
print ("MaxEnt, Bi-Gram TF-IFD: ", accuracy)


# SVM
accuracy = train_model(svm.SVC(kernel='linear'), train_text_tfidf, train_label, test_text_tfidf,test_label)
print ("SVM, WordLevel TF-IDF: ", accuracy)
accuracy = train_model(svm.SVC(kernel='linear'), train_text_tfidf_bigram, train_label, test_text_tfidf_bigram,test_label)
print ("SVM, Bi-Gram TF-IFD: ", accuracy)


#Renault stocktwits lexicons: download lexicons from: http://www.thomas-renault.com
l2dic=pd.read_csv(r'Path/l2_lexicon.csv',sep=';', error_bad_lines=False, index_col=False, dtype='unicode') 
key = l2dic["keyword"].values
val = l2dic["sentiment"].values
for i in range(len(val)):
    val[i]=int(val[i].replace('positive','1').replace('negative','-1'))
zipbObj = zip(key, val) 
l2dict = dict(zipbObj)

l1dic=pd.read_csv(r'path/l1_lexicon.csv',sep=',', error_bad_lines=False, index_col=False, dtype='unicode') 
key = l1dic["keyword"].values
val = l1dic["sw"].values.astype('float64')
zipbObj = zip(key, val) 
l1dict = dict(zipbObj)

#Loughran-McDonald Master Dictionary: Download lexicons from: https://sraf.nd.edu/textual-analysis/resources/
lmdic=pd.read_csv(r'path/lmdic.csv',sep=',', error_bad_lines=False, index_col=False, dtype='unicode') 
key = lmdic["keyword"].values
val = lmdic["sentiment"].values.astype('int')
zipbObj = zip(np.char.lower(list(key)), val) 
lmdict = dict(zipbObj)


def lmdic_model(dic,text,test):
    predictions=[]
    for txt in text:
        score=0
        tokens = txt.split(' ')
        for word in tokens:
            if 'negtag' in word:
                word = word.replace("negtag","")
                if word in dic:
                    score = score + dic.get(word)*-1
            else:
                if word in dic:
                    score = score + dic.get(word)
                    
        if score > 0:
            predictions.append(1)
        elif score < 0 :
            predictions.append(0)
        elif score == 0:
            predictions.append(2)
    return metrics.accuracy_score(predictions, test),predictions

def lxdic_model(dic,text,ngram,test):
    predictions=[]
    for txt in text:
        score=0
        txt=txt.replace('negtag','negtag_')
        tokens = txt.split(' ')
        if ngram ==1:
            for word in tokens:                
               if word in dic:
                    score = score + dic.get(word)
        if ngram==2:
            bigram = list(ngrams(tokens, 2))
            for i in range(len(bigram)):
                bigram[i] = ' '.join([w for w in bigram[i]])
            for word in bigram:
               if word in dic:
                    score = score + dic.get(word)
            for word in tokens:
               if word in dic:
                    score = score + dic.get(word)
                    
        if score > 0:
            predictions.append(1)
        elif score < 0 :
            predictions.append(0)
        elif score == 0:
            predictions.append(2)
    return metrics.accuracy_score(predictions, test),predictions

#VADER sentiment classification:https://github.com/cjhutto/vaderSentiment
def vader_model(text,test):
    predictions=[]
    for txt in text:
        score = analyser.polarity_scores(txt)
                           
        if score.get('compound') >= 0.05:
            predictions.append(1)
        elif score.get('compound') <= -0.05 :
            predictions.append(0)
        else:
            predictions.append(2)
    return metrics.accuracy_score(predictions, test),predictions
                         


acc,pre=lxdic_model(l1dict,test_text,1,test_label)
print(acc)
acc,pre=lxdic_model(l2dict,test_text,1,test_label)
print(acc)
acc,pre=lxdic_model(l1dict,test_text,2,test_label)
print(acc)
acc,pre=lxdic_model(l2dict,test_text,2,test_label)
print(acc)
acc,pre=lmdic_model(lmdict,test_text,test_label)
print(acc)      
acc,pre=vader_model(test_text,test_label)
print(acc)
