# Stock Microblog Scraping and Text Sentiment Analysis

Gulinuer Azhati (Last update 02.03.2020)

The current project aims to gather and analysis stock related microblogs that are collected from Twitter and StockTwits.

Web Crawlers are developed with Python Tweepy, Selenium and BeautifulSoup libraries to extract all messages posted on Twitter (Twitter API data downloader for recent 1 week tweets, web scraper for historical tweet dwonloading) and StockTwits (1 scraper for recent, another one for historical post searching).
For each message, the following information is collected: 1) dates, converted to 'US/Eastern' time zone, with a one-minute granularity, 2) the message poster’s username, 3) the user’s followers count, 4) the message content, 5) the message retweet count and like count, 6) the message sentiment tag (if exist, record “bullish” and “bearish” tags, only for StockTwits).

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## The text data preprocessing
To improve the quality of the text corpus, a data cleaning process is implemented (Angiani et al. 2016; Renault, 2017; Sprenger et al., 2014):
* Lower case all letters
* Remove stop words (get rid of “a”, “an”, “the”, “rt”)
* Clean all hyperlinks
* Clean all Hashtags (#...) 
* Clean all user mentions (@...) 
* Clean all Cashtags ($...)
* Replace all emoji and smiley icons: Positive, Negative and Other icons to “emojipos”, “emojineg” and “emoicon” repectively 
* Replace slangs (e.g., “cuz” -> “cause”, “l8” -> “late”, “plz” -> “please” etc.)
* Clean extra vowels (e.g., ‘cooool’, ‘craaaaazy’ etc.)
* Replace badwords with tag “badword” (e.g., “shit”, “asshole” etc)
* Replace positive numbers (including 0) with tag ‘posnum’ and negative numbers with ‘negnum’
* Remove the punctuations and just keep  ! ? % + – = 
* Deal with negations 
* Remove the single letters
* Remove extra white spaces



## The supervised classification models

Using machine learning models to determine the emotion class of each tweet is a classification problem which aim to train a mapping function f : tweet -> class{bullish, bearish}. The mapping function f is based on features extracted from training corpus F = {f1, f2,f3 …fn}. In this project, several supervised machine learning classifiers are trained and compared, including naive Bayesian classification (NB), Logistic Regression (LR) and Support Vector Machine (SVM) (Antweiler and Frank, 2004; Colas and Brazdil, 2006; Pranckevičius and Marcinkevičius, 2017).

Implementation. Using Python sklearn library, three classification algorithms are perform with parameters: 1) applying document tfidf score instead of word count ; 2) to avoid possible corpus specific stop words, exclude words appeared in more than 90% training posts (i.e., max_df =0.9); 3) to remove possible misspellings, exclude words appeared in less than 0.1% posts (min_df=0.0001 or in other words, the feature should appear at least 10 times); 4) tried both unigram (each word as a feature, e.g, “buy”) and bigram (two consecutive words are one feature, e.g., “strong buy”, “buy ?”).
## The unsupervised classification techniques

Some lexicon and rule-based techniques are also used in text emotion classifications, which can avoid model training. Here, we’ll try VADER (Hutto and Gilbert, 2014), 
Loughran–McDonald finance-text dictionary (Loughran and Mcdonald, 2011) and lexicons created by Renault (2017) based on StockTwits posts.

* [VADER (Valence Aware Dictionary and sEntiment Reasoner)](https://github.com/cjhutto/vaderSentiment): is specifically to classify emotions expressed in social media (Hutto and Gilbert, 2014) implemented in python . 
* [Loughran-McDonald Master Dictionary (LM)](https://sraf.nd.edu/textual-analysis/resources/) (Loughran and Mcdonald, 2011): aim at tailoring the finance-related text analysis. With experts’ manual classification, this dictionary provides lexicon list consist of 2007 negative terms (sentiment score -1) and 1626 positive terms (sentiment score 1). Text can be classified into positive or negative categories based on the total score of the terms that are matching with the dictionary terms .
* [Renault (2017) StockTwits lexicons](http://www.thomas-renault.com) : In his study, Renault using 375,000 bullish and 375,000 bearish labelled tweets trained 2 lexicon lists. Weighted field-specific lexicon (WFL): 4000 negative terms and 4000 positive terms, term score = (occurrence in bullish posts – occurrence in bearish posts) / total occurrence. Manual field-specific lexicon (MFL): manually cleaned list consist of 768 negative terms (score -1) and 543 positive terms (score 1). Text can be classified into positive or negative categories based on the total score of the terms that are matching with the lexicon list terms.


## Related Literature
* Angiani, G., Ferrari, L., Fontanini, T., Fornacciari, P., Magliani, F., & Manicardi, S. (2016). A Comparison between Preprocessing Techniques for Sentiment Analysis in Twitter. In KDWeb, 11.
* Antweiler, & Frank. (2004). Is All That Talk Just Noise? The Information Content of Internet Stock Message Boards. The Journal of Finance, 59(3), 1259–1294.
* Colas, F., & Brazdil, P. (2006). Comparison of SVM and Some Older Classification Algorithms in Text Classification Tasks. In M. Bramer (Ed.), Artificial Intelligence in Theory and Practice (Vol. 217, pp. 169–178). Springer US. https://doi.org/10.1007/978-0-387-34747-9_18
* Hutto, C. J., & Gilbert, E. (2014). VADER: A Parsimonious Rule-based Model for Sentiment Analysis of Social Media Text. 8th International AAAI Conference on Weblogs and Social Media, 10.
* Loughran, T., & Mcdonald, B. (2011). When Is a Liability Not a Liability? Textual Analysis, Dictionaries, and 10-Ks. The Journal of Finance, 66(1), 35–65. https://doi.org/10.1111/j.1540-6261.2010.01625.x
* Pranckevičius, T., & Marcinkevičius, V. (2017). Comparison of Naive Bayes, Random Forest, Decision Tree, Support Vector Machines, and Logistic Regression Classifiers for Text Reviews Classification. Baltic Journal of Modern Computing, 5(2). https://doi.org/10.22364/bjmc.2017.5.2.05
* Renault, T. (2017). Intraday online investor sentiment and return patterns in the U.S. stock market. Journal of Banking & Finance, 84, 25–40. https://doi.org/10.1016/j.jbankfin.2017.07.002
* Sprenger, T. O., Tumasjan, A., Sandner, P. G., & Welpe, I. M. (2014). Tweets and Trades: The Information Content of Stock Microblogs: Tweets and Trades. European Financial Management, 20(5), 926–957. https://doi.org/10.1111/j.1468-036X.2013.12007.x

