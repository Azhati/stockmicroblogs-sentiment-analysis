from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
import sys
import time
from datetime import datetime
import dateutil.parser
from pandas.tseries.holiday import AbstractHolidayCalendar, Holiday, nearest_workday, \
    USMartinLutherKingJr, USPresidentsDay, GoodFriday, USMemorialDay, \
    USLaborDay, USThanksgivingDay


class USTradingCalendar(AbstractHolidayCalendar):
    rules = [
        Holiday('NewYearsDay', month=1, day=1, observance=nearest_workday),
        USMartinLutherKingJr,
        USPresidentsDay,
        GoodFriday,
        USMemorialDay,
        Holiday('USIndependenceDay', month=7, day=4, observance=nearest_workday),
        USLaborDay,
        USThanksgivingDay,
        Holiday('Christmas', month=12, day=25, observance=nearest_workday),
        Holiday('BushSeniorDeath', year=2018, month=12, day=5)
        # added BushSeniors Death Mourning holiday on 5th December 2018, see Mail from December 3, 2018
    ]

def is_trading_day(date):
    inst = USTradingCalendar()
    holidays = inst.holidays(datetime(date.year-1, 12, 31), datetime(date.year, 12, 31))

    return date not in holidays and date.weekday() < 5


#Input variables:
    #query, list of string, the ticker of the stocks to be scraped, e.g. ["BA"]
    #scroll, int, the number of the page to be scrolled, to scrap more historical post, more pages need to be scrolled, e.g., scroll =1000
    #date_since, the limit date for scraping posts, e.g., '2019-01-01', then scraping will stop after reaching this date, otherwise scroll will be continue
    #userdict: dictionary, initial userdict={}, after each scrap, the user name and follower counts will be returned and can be used for next scrap
def get_tweet(query, scroll, date_since,userdict):
    browser=webdriver.Firefox(executable_path=r'path/geckodriver.exe') #Downlaod firefox webdriver (https://github.com/mozilla/geckodriver/releases), then enter the local path
    browser.get("https://stocktwits.com")
    browser.find_elements_by_xpath("//*[contains(text(), 'Accept Cookies')]")[1].click()
    time.sleep(2)
    browser.find_elements_by_xpath("//*[contains(text(), 'Log In')]")[0].click()
    time.sleep(2)
    browser.find_elements_by_xpath("//input[@type='text'][@name='login']")[0].send_keys("XXXXX") #input username
    time.sleep(2)
    browser.find_elements_by_xpath("//input[@type='password'][@name='password']")[0].send_keys("XXXXX") #input password
    time.sleep(2)
    browser.find_elements_by_xpath("//button[@type='submit'][contains(text(), 'Log In')]")[0].click()
    time.sleep(2)
        
    for q in query:
        browser.get("https://stocktwits.com/symbol/"+q)
        time.sleep(2)
        
        #driver.get("https://stocktwits.com/symbol/"+q)
        for i in range(0,scroll):  #scroll down X times
            browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(2)
            sys.stderr.write('\r{0}/{1} complete...'.format(i+1,scroll))
            
                
    
        content = browser.page_source
        soup = BeautifulSoup(content)
    
        tweets = soup.findAll('div',attrs={'class':'st_1m1w96g st_1x3QBA7 st_1SZeGna st_3MXcQ5h st_3-tdfjd'})
    
        sentiments = []
        dates = []
        year=[]
        month =[]
        day=[]
        hour=[]
        minute=[]
        is_trade=[]
        twits = []
        likes = []
        user_ids = []
        followers = []
        company = []
        for a in tweets:
            #Get tweet posting time
            try:
                date = a.find('a', href = True, attrs={'class':'st_28bQfzV st_1E79qOs st_3TuKxmZ st_3Y6ESwY st_GnnuqFp st_1VMMH6S'}).text

            except:
                date = None
            
            if date is not None and dateutil.parser.parse(date) > datetime.strptime(date_since, '%Y-%m-%d'): #and datetime.strptime(date.replace('th,', ' 2019').replace(' pm','PM').replace(' am', 'AM'), '%b %d %Y %I:%M%p')< datetime.strptime(date_until, '%b %d %Y %I:%M%p'):
                try:
                    ts = dateutil.parser.parse(date)
                    dates.append(ts)
                    year.append(ts.year)
                    month.append(ts.month)
                    day.append(ts.day)
                    hour.append(ts.hour)
                    minute.append(ts.minute)
                    is_trade.append(is_trading_day(ts))                
                except:
                    dates.append(None)
                    
                #Get sentiment tags
                senti = a.find('div', attrs={'class':'lib_XwnOHoV lib_3UzYkI9 lib_lPsmyQd lib_2TK8fEo'})
                try:
                    sentiments.append(senti.text)
                except:
                    sentiments.append(None)
                           
                #Get tweet content        
                twit = a.find('div', attrs={'class':'st_3SL2gug'})
                try:
                    twits.append(twit.text)
                except:
                    twits.append(None)
                
                #Get like counts        
                like = a.find('span', attrs={'class':'st_1tZ744c st_VNaOUo1 st_39Uuz7b st_VNaOUo1 st_3OfMfdC'})
                try:
                    likes.append(like.text)
                except:
                    likes.append(None)
                
                #user related
                user = a.find('a', href = True, attrs={'class':'st_3ADU0hJ'})
                try:
                    user_id = user.attrs['href']
                    user_ids.append(user_id)
                    if user_id in userdict:
                        followers.append(userdict.get(user_id))
                    else:
                        try:
                            browser.get("https://stocktwits.com"+user_id)
                            time.sleep(2)
                            user_page = BeautifulSoup(browser.page_source)
                            follower = user_page.findAll('h2', attrs ={'class':"st__tZJhLh st_29vRgrA st_2LcBLI2 st_2h5TkHM st_8u0ePN3 st_2mehCkH st_CjvTpBY st_2z6yeWY"})[2]
                            followers.append(follower.text)
                            userdict[user_id] = follower.text
                            
                        except:
                            followers.append(None)
                except:
                    user_ids.append(None)
                    followers.append(None)
                
                #Company
                company.append(q)
            
        print(q+str(dates[-1])+' ' + str(len(tweets)))
        d = {'user_ids': user_ids,
                 'tweets': twits,
                 'date': dates,
                 'sentiments':sentiments,
                 'likes': likes,
                 'followers': followers,
                 'company':company,
                 'year':year,
                 'month':month,
                 'day':day,
                 'hour':hour,
                 'minute':minute,
                 'is_trade':is_trade
                         }
        
        df = pd.DataFrame(data = d)
            
        df.to_csv (r'path.csv', index = None, header= False, mode = 'a')
    browser.close()
    return userdict



date_since ='2019-11-01'
scroll1 = 12
userdict={}
query= ["TSLA", "AAPL","AMZN", "AMD","MSFT","NFLX","BA","SNAP"]
userdict = get_tweet(query, scroll1, date_since,userdict)
