import tweepy as tw
consumer_key = "********"
consumer_secret = "********"
access_token = "********"
access_token_secret = "********"
import numpy as np
import pandas as pd
import json
import sys
import re
import time
from datetime import datetime
import dateutil.parser
from pytz import timezone
from selenium import webdriver
from bs4 import BeautifulSoup
from preprocess import preprocess
from pandas.tseries.holiday import AbstractHolidayCalendar, Holiday, nearest_workday, \
    USMartinLutherKingJr, USPresidentsDay, GoodFriday, USMemorialDay, \
    USLaborDay, USThanksgivingDay



class USTradingCalendar(AbstractHolidayCalendar):
    rules = [
        Holiday('NewYearsDay', month=1, day=1, observance=nearest_workday),
        USMartinLutherKingJr,
        USPresidentsDay,
        GoodFriday,
        USMemorialDay,
        Holiday('USIndependenceDay', month=7, day=4, observance=nearest_workday),
        USLaborDay,
        USThanksgivingDay,
        Holiday('Christmas', month=12, day=25, observance=nearest_workday),
        Holiday('BushSeniorDeath', year=2018, month=12, day=5)
        # added BushSeniors Death Mourning holiday on 5th December 2018, see Mail from December 3, 2018
    ]

def is_trading_day(date):
    inst = USTradingCalendar()
    holidays = inst.holidays(datetime(date.year-1, 12, 31), datetime(date.year, 12, 31))

    return date not in holidays and date.weekday() < 5




#Twitter API for Downloading recent up to 1 week tweets according to given search keyword, start and end date
 
def tweets_to_df(api,search, date_since,date_until):

    tweets = tw.Cursor(api.search,q=search,
              lang="en",
              since=date_since,
              until=date_until,
              #count=100000,
              tweet_mode='extended').items(50000)
    user_ids=[]
    text=[]
    date=[]
    likes=[]
    followers=[]
    company = []
    year =[]
    month=[]
    day = []
    hour =[]
    minute =[]
    is_trade =[]
    retweet_count=[]
    retweeted =[]
    i=0
    for tweet in tweets:
        tweet=json.dumps(tweet._json)
        tweet=json.loads(tweet)
        
        #text
        if 'retweeted_status' in tweet:
            retweeted.append(1)
            text.append(tweet['retweeted_status']['full_text'])
        else:
            retweeted.append(0)
            text.append(tweet['full_text'])
        
        #created_date
        ts=dateutil.parser.parse(tweet['created_at'])
        date.append(ts)
        year.append(ts.year)
        month.append(ts.month)
        day.append(ts.day)
        hour.append(ts.hour)
        minute.append(ts.minute)
        is_trade.append(is_trading_day(ts))   
        
        #like counts
        likes.append(tweet['favorite_count'])
        
        #user related
        user_ids.append(tweet['user']['screen_name'])
        followers.append(tweet['user']['followers_count'])
        
        #retweet count
        retweet_count.append(tweet['retweet_count'])
        
        #company
        company.append(search.lower().replace('$',''))
        
        sys.stderr.write('\r{0}/{1} complete...'.format(i+1,len(tweets)))
        i=i+1
    print(search + ' ' + str(len(date)))
    d = {'user_ids': user_ids,
                 'tweets': text,
                 'date': date,
                 'likes': likes,
                 'followers': followers,
                 'company':company,
                 'year':year,
                 'month':month,
                 'day':day,
                 'hour':hour,
                 'minute':minute,
                 'is_trade':is_trade,
                 'retweet_count':retweet_count,
                 'retweeted':retweeted                 
                         }
        
    df = pd.DataFrame(data = d)
            
    df.to_csv (r'****.csv', index = None, header= False, mode = 'a')
    
# Creating the authentication object
auth = tw.OAuthHandler(consumer_key, consumer_secret)
# Setting your access token and secret
auth.set_access_token(access_token, access_token_secret)
# Creating the API object while passing in auth information
api = tw.API(auth,wait_on_rate_limit=True, wait_on_rate_limit_notify=True, compression=True) 

date_since='2020-03-01'
date_until='2020-03-05'
query=['$TSLA', '$AAPL', '$AMZN', '$AMD',
 '$MSFT', '$NFLX', '$BA', '$SNAP',
 '$MO', '$TWTR', '$FB', '$NVDA',
 '$DIS', '$MU', '$TGT', '$GE',
 '$JPM', '$M', '$BAC', '$COST',
 '$SBUX', '$INTC', '$F', '$ABBV',
 '$CSCO', '$GS', '$C', '$MCD',
 '$WMT', '$HD',  '$XOM',
 '$PFE', '$MRK', '$JNJ', '$IBM',
 '$WFC', '$PG','$MA', '$MS',
 '$CMG', '$KO', '$QCOM', '$AMGN', '$GD','$VZ']

for s in query:
    tweets_to_df(api,s, date_since,date_until)
    


#Scraping historical tweets according search query, start and end date
#Input variable: 
#query:  search keyword, e.g. "$BA"    
#scroll: initial scroll time, int, e.g. 100
#start and end dates, srting, e.g. "2018-12-01"
#userdict, dictionary, initial userdict = {}, then collect the scraped users' follower counts and can be used for the following scraping to save time
def scrap_tweets(query,scroll,date,userdict):
    date_since = date[0]
    date_until=date[1]
    c=1
    while c<20:
        try:
            browser=webdriver.Firefox(executable_path=r'path/geckodriver.exe') # downlaod firefox webdriver (https://github.com/mozilla/geckodriver/releases), then enter the local path
            browser.get("https://twitter.com")
            time.sleep(c)
            browser.find_elements_by_xpath("//a[contains(text(), 'Log in')]")[0].click()
            time.sleep(2)
            browser.find_elements_by_xpath("//input[@type='text'][@name='session[username_or_email]']")[1].send_keys("XXXXXX") #input username
            time.sleep(2)
            browser.find_elements_by_xpath("//input[@type='password'][@name='session[password]']")[1].send_keys("XXXXXX") #input password
            time.sleep(2)
            browser.find_elements_by_xpath("//button[@type='submit'][contains(text(), 'Log in')]")[0].click()
            time.sleep(2)
            browser.get("https://twitter.com/search?q=%24{}%20lang%3Aen%20until%3A{}%20since%3A{}&src=typed_query&f=live".format(query,date_until,date_since))
            time.sleep(2)
            break
        except:
            print(c)
            c=c+1
            browser.close()
            
    content = browser.page_source
    soup = BeautifulSoup(content)
    tweets = soup.findAll('div',attrs={'data-testid':'tweet'})
    
    last_height = browser.execute_script("return document.body.scrollHeight")
    y=1000
    for i in range(scroll):
        # Scroll down to bottom
        browser.execute_script("window.scrollTo(0, "+str(y)+")")
        try:
            html_source = browser.page_source
            content=html_source.encode('utf-8')
            soup = BeautifulSoup(content)
            tweets.extend(soup.body.findAll('div',attrs={'data-testid':'tweet'}))
            time.sleep(2)
            y += 1000
        except:
            y += 1000
        
        sys.stderr.write('\r{0}/{1} complete...'.format(i+1,scroll))
    new_height = browser.execute_script("return document.body.scrollHeight")
             
    # Calculate new scroll height and compare with last scroll height
    
    while True:
        if new_height == last_height:
            break
        else:
            last_height=new_height
            for i in range(10):        
                # Scroll down to bottom
                browser.execute_script("window.scrollTo(0, "+str(y)+")")
                try:
                    html_source = browser.page_source
                    content=html_source.encode('utf-8')
                    soup = BeautifulSoup(content)
                    tweets.extend(soup.body.findAll('div',attrs={'data-testid':'tweet'}))
                    time.sleep(2)
                    y += 1000
                except:
                    y += 1000
                sys.stderr.write('\r{0}/{1} complete...'.format(i+1,10))
            
            # Calculate new scroll height and compare with last scroll height
            new_height = browser.execute_script("return document.body.scrollHeight")
           
                
    
    user_ids=[]
    text=[]
    dates=[]
    likes=[]
    followers=[]
    company = []
    year =[]
    month=[]
    day = []
    hour =[]
    minute =[]
    is_trade =[]
    retweet=[]
    i=0
    for a in tweets:
        try:
            date = a.find('time').attrs['datetime']
            ts = dateutil.parser.parse(date)
            ts=ts.astimezone(timezone('US/Eastern'))
            dates.append(ts)
            year.append(ts.year)
            month.append(ts.month)
            day.append(ts.day)
            hour.append(ts.hour)
            minute.append(ts.minute)
            is_trade.append(is_trading_day(ts))                
        except:
            dates.append(None)
                    
            
        #Get tweet content        
        try:
            twit = a.find('div',attrs={'class':'css-901oao r-hkyrab r-1qd0xha r-a023e6 r-16dba41 r-ad9z0x r-bcqeeo r-bnwqim r-qvutc0'}).get_text(strip=True)
            text.append(twit)
        except:
            text.append(None)
                
        #Get like counts        
        try:
            likes.append(a.find('div', attrs={'aria-label':'Like'}).get_text(strip=True))
        except:
            likes.append(None)
        
        #Get retweet counts        
        try:
            retweet.append(a.find('div', attrs={'aria-label':'Retweet'}).get_text(strip=True))
        except:
            retweet.append(None)
            
        #user related
        try:
            user_id = a.find('div', attrs={'class':'css-1dbjc4n r-18u37iz r-1wbh5a2 r-1f6r7vd'}).get_text(strip=True)
            user_id=re.sub(r'^@','',user_id)
            user_ids.append(user_id)
            if user_id in userdict:
                followers.append(userdict.get(user_id))
            else:
                try:
                    browser.get("https://twitter.com/"+user_id)
                    time.sleep(2)
                    user_page = BeautifulSoup(browser.page_source)
                    follower = user_page.find('div', attrs ={'class':"css-1dbjc4n r-18u37iz"}).get_text(strip=True).split('Following')[1].replace('Followers','')
                    followers.append(follower)
                    userdict[user_id] = follower
                    
                except:
                    followers.append(None)
        except:
            user_ids.append(None)
            followers.append(None)
                
        #Company
        company.append(query)
        sys.stderr.write('\r{0}/{1} complete...'.format(i+1,len(tweets)))
        i+=1
            
    print(query+str(dates[-1])+' ' + str(len(tweets)))
    d = {'company':company,
         'date': dates,
         'day':day,
         'followers': followers, 
         'hour':hour,                 
         'is_trade':is_trade,
         'likes': likes,
         'minute':minute,
         'month':month,         
         'retweet_count':retweet,
         'tweets': text,
         'user_ids': user_ids,
         'year':year}
    df = pd.DataFrame(data = d)
    df=df.drop_duplicates(keep='first')
            
    df.to_csv (r'path.csv', index = None, header= False, mode = 'a') #save to local path
    browser.close()
    return userdict



query=['HPQ','HON','VZ','AXP',
       'IP','AA','MMM','NKE',
       'CVX','CAT','PG','IBM',
       'KO','JNJ','MRK','PFE', 
       'XOM','HD','C','MCD',
       'GD','WMT','BAC','UTX',
       'GE','JPM','MO','DIS']
userdict={}

for s in query:
    userdict=scrap_tweets(s,200,'2018-09-01','2018-10-01',userdict)

