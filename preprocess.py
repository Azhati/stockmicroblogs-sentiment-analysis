import emojis
import re
from nltk.tokenize import TweetTokenizer
twk = TweetTokenizer()
#from sentimentcorpus import sent_corp
#snt = sent_corp(3)

def smiley_cleaner(tweet):
    #A Comparison between Preprocessing Techniques for Sentiment Analysis in Twitter
    #https://github.com/fmaglia/SA_cleaners
	"""convert the emoticons in the relative sentiment group"""
	clean_text = tweet
	
	#the list of emoticons and theirs convertion in unicode are found on Wikipedia
	
	#angel, innocent
	clean_text = re.sub(r"O:\-\)|0:\-3| 0:3 |0:\-\)|0:\)|0;\^\)"," smile_angel ",clean_text)
	#evil
	clean_text = re.sub(r">:\)|>;\)|>:\-\)|\}:\-\)|}:\)|3:\-\)|3:\)"," smile_evil ",clean_text)
	#happy
	clean_text = re.sub(r":\-\)|:\)|:D|:o\)|:\]| :3|:c\)|:>| =\]|8\)|=\)|:\}|:\^\)"," smile_happy ",clean_text)
	#laugh
	clean_text = re.sub(r":\-D|8\-D|8D|x\-D|xD|X\-D|XD|=\-D|=D|=\-3| =3 |B\^D|:\-\)\)|:'\-\)|:'\)"," smile_laugh ",clean_text)
	#angry
	clean_text = re.sub(r":\-\|\||:@| >:\("," smile_angry ",clean_text)
	#sad
	clean_text = re.sub(r">:\[|:\-\(|:\(|:\-c|:c|:\-<|:\-\[|:\[|:\{| <\\3 "," smile_sad " ,clean_text)
	#crying
	clean_text = re.sub(r";\(|:'\-\(|:'\("," smile_crying ",clean_text)
	#horror, disgust
	clean_text = re.sub(r"D:<|D:|D8| D; |D=|DX|v\.v|D\-':"," smile_horror ",clean_text)
	#surprise, shock
	clean_text = re.sub(r">:O|:\-O|:O|:\-o|:o|8\-0|O_O|o\-o|O_o|o_O|o_o|O\-O"," smile_surprise ",clean_text)
	#kiss
	clean_text = re.sub(r":\*|:\-\*|:\^\*|\( '\}\{' \)|<3"," smile_kiss ",clean_text)
	#winking
	clean_text = re.sub(r";\-\)|;\)|\*\-\)|\*\)|;\-\]|;\]|;D|;\^\)|:\-,"," smile_wink ",clean_text)
	#tongue sticking out
	clean_text = re.sub(r">:P|:\-P|:P|X\-P|x\-p| xp | XP |:\-p| :p | =p |:\-Þ|:Þ|:þ|:\-þ|:\-b| :b | d: "," smile_tongue_sticking_out ",clean_text)
	#skeptical
	clean_text = re.sub(r">:\\|>:/|:\-/|:\-\.|:/|:\\|=/|=\\| :L | =L | :S |>\.<"," smile_skeptical ",clean_text)
	#straight face
	clean_text = re.sub(r":\||:\-\|"," smile_straight_face ",clean_text)

	
	clean_text = re.sub(r"(smile_angel|smile_surprise|smile_happy|smile_laugh|smile_kiss|smile_wink|smile_tongue_sticking_out)", " emojipos ",clean_text)
	clean_text = re.sub(r"(smile_evil|smile_angry|smile_sad|smile_crying|smile_horror|smile_skeptical|smile_straight_face)"," emojineg ",clean_text)

	return clean_text
    


    
def emoji_cleaner(tweet):
    clean_text = tweet
    clean_text = re.sub(r'(🚀|🔥|📈|🤑|💰|💸|✅|💥|🌈|😆|😊|😃|☺️|😏|😍|😘|😚|😌|😆|😁|😉|😜|😝|😀|😗|😙|😛|😋|😎|💛|💙|💜|❤️|💚|💓|👍|👍|👌|✌️|👏|💪|💪🏼|🤗|🐂|🐮)',' emojipos ',clean_text)
    clean_text = re.sub(r'(📉|💀|👋|⛷|😟|😦|😧|😮|😬|😕|😯|😒|😓|😥|😩|😔|😞|😖|😨|😰|😣|😢|😭|😲|😱|😫|😠|😡|😤|😪|😷|😵|👿|😈|💔|💢|💩|💩|💩|👎|👎|🖕|🤬)',' emojineg ',clean_text)
    
    tmp=list(emojis.get(clean_text))
    if len(tmp) >0:
        for i in tmp:
            clean_text = re.sub(i,' emoicon ',clean_text)
    return clean_text
    
        

def slang_cleaner(tweet):
    clean_text = tweet
    #twk = TweetTokenizer()
    clean_text = re.sub(' tnx ',' thanks ', clean_text)
    clean_text = re.sub(' thx ',' thanks ', clean_text)
    clean_text = re.sub(' u ',' you ', clean_text)
    clean_text = re.sub(' kk ', ' ok ', clean_text)
    clean_text = re.sub(' lulz ', ' lol ', clean_text)
    clean_text = re.sub(' sry ', ' sorry ', clean_text)
    clean_text = re.sub(' l8 ', ' late ', clean_text)
    clean_text = re.sub(' w8 ', ' wait ', clean_text)
    clean_text = re.sub(' m8 ', ' mate ', clean_text)
    clean_text = re.sub(' gr8 ', ' great ', clean_text)
    clean_text = re.sub(' plz ', ' please ', clean_text)
    clean_text = re.sub(' pls ', ' please ', clean_text)
    clean_text = re.sub(' 2moro ', ' tomorrow ', clean_text)
    clean_text = re.sub(' somthin ', ' something ', clean_text)
    clean_text = re.sub(' srsly ', ' seriously ', clean_text)
    clean_text = re.sub(' sht ', ' shit ', clean_text)
    clean_text = re.sub(' imho ', ' imo ', clean_text)
    clean_text = re.sub(' omfg ', ' omg ', clean_text)
    clean_text = re.sub(' bros ', ' brother ', clean_text)
    clean_text = re.sub(' bro ', ' brother ', clean_text)
    clean_text = re.sub(' nope ', ' no ', clean_text)
    clean_text = re.sub(' cuz ', ' because ', clean_text)
    clean_text = re.sub(' cud ', ' could ', clean_text)
    
    return clean_text

def badword_cleaner(tweet):
    clean_text = tweet
    

    clean_text = re.sub(' fuck you ',' badword ', clean_text)
    clean_text = re.sub(' fuck off ', ' badword ', clean_text)
    clean_text = re.sub(' faggot ', ' badword ', clean_text)
    clean_text = re.sub(' son of a bitch ', ' badword ', clean_text)
    clean_text = re.sub(' bastard ', ' badword ', clean_text)
    clean_text = re.sub(' asshole ', ' badword ', clean_text)
    clean_text = re.sub(' scumbag ', ' badword ', clean_text)
    clean_text = re.sub(' whore ', ' badword ', clean_text)
    clean_text = re.sub(' idiot ', ' badword ', clean_text)
    clean_text = re.sub(' dickhead ', ' badword ', clean_text)
    clean_text = re.sub(' dumbass ', ' badword ', clean_text)
    clean_text = re.sub(' fatass ', ' badword ', clean_text)
    clean_text = re.sub(' motherfucker ', ' badword ', clean_text)
    clean_text = re.sub(' pussy ', ' badword ', clean_text)
    clean_text = re.sub(' sucker ', ' badword ', clean_text)
    clean_text = re.sub(' twat ', ' badword ', clean_text)
    clean_text = re.sub(' shit ', ' badword ', clean_text)
    clean_text = re.sub(' shiit ', ' badword ', clean_text)
    
    return clean_text


def neg_cleaner(tweet):
    
    clean_text = tweet
    
    #substitute the negation with "not"	
    clean_text = re.sub(r" isn’t "," not ",clean_text)
    clean_text = re.sub(r" isnt "," not ",clean_text)
    clean_text = re.sub(r" aren’t "," not ",clean_text)
    clean_text = re.sub(r" ain’t "," not ",clean_text)
    clean_text = re.sub(r" arent "," not ",clean_text)
    clean_text = re.sub(r" aint "," not ",clean_text)
    clean_text = re.sub(r" don’t "," not ",clean_text)
    clean_text = re.sub(r" dont "," not ",clean_text)
    clean_text = re.sub(r" didn’t "," not ",clean_text)
    clean_text = re.sub(r" didnt "," not ",clean_text)
    clean_text = re.sub(r" doesn’t "," not ",clean_text)
    clean_text = re.sub(r" doesnt "," not ",clean_text)
    clean_text = re.sub(r" haven’t "," not ",clean_text)
    clean_text = re.sub(r" hasn’t "," not ",clean_text)
    clean_text = re.sub(r" hadn’t "," not ",clean_text)
    clean_text = re.sub(r" wasn’t "," not ",clean_text)
    clean_text = re.sub(r" weren’t "," not ",clean_text)	
    clean_text = re.sub(r" won’t "," not ",clean_text)
    clean_text = re.sub(r" havent "," not ",clean_text)
    clean_text = re.sub(r" hasnt "," not ",clean_text)
    clean_text = re.sub(r" hadnt "," not ",clean_text)
    clean_text = re.sub(r" wasnt "," not ",clean_text)
    clean_text = re.sub(r" werent "," not ",clean_text)	
    clean_text = re.sub(r" wont "," not ",clean_text)
    clean_text = re.sub(r" can’t "," not ",clean_text)
    clean_text = re.sub(r" cant "," not ",clean_text)
    clean_text = re.sub(r" cannot "," not ",clean_text)
    clean_text = re.sub(r" couldn’t "," not ",clean_text)
    clean_text = re.sub(r" couldnt "," not ",clean_text)
    clean_text = re.sub(r" wouldn’t "," not ",clean_text)
    clean_text = re.sub(r" shouldn’t "," not ",clean_text)
    clean_text = re.sub(r" shan’t "," not ",clean_text)
    clean_text = re.sub(r" wouldnt "," not ",clean_text)
    clean_text = re.sub(r" shouldnt "," not ",clean_text)
    clean_text = re.sub(r" shant "," not ",clean_text)
    clean_text = re.sub(r" daren’t "," not ",clean_text)
    clean_text = re.sub(r" darent "," not ",clean_text)
    clean_text = re.sub(r" mightn’t "," not ",clean_text)
    clean_text = re.sub(r" mightnt "," not ",clean_text)
    clean_text = re.sub(r" mustn’t "," not ",clean_text)
    clean_text = re.sub(r" mustnt "," not ",clean_text)
    clean_text = re.sub(r" needn’t "," not ",clean_text)
    clean_text = re.sub(r" neednt "," not ",clean_text)
    clean_text = re.sub(r" oughtn’t "," not ",clean_text)
    clean_text = re.sub(r" oughtnt "," not ",clean_text)
    
    
    tokens = twk.tokenize(clean_text)
    for i in range(len(tokens)):
        if tokens[i] in ['not', 'no', 'none', 'nope','neither','never','nobody']:
            j=i+1
            while j <len(tokens) and tokens[j] not in [',','.','?','!',')','"',':',';','not', 'no', 'none', 'neither', 'never', 'nobody']:
                tokens[j] = 'negtag_' + tokens[j]
                j=j+1
    clean_text = ' '.join(tokens)
    
    return clean_text

def vv_cleaner(tweet):
	"""remove punctuation repeated and introduce spaces between punctuation"""
	clean_text = tweet
	
	##add multistop tag when multiple punctuation signs are found
	#clean_text = re.sub(r"!!+"," multi_exclamation ",clean_text)
	#clean_text = re.sub(r"\?\?+"," multi_interrogation ",clean_text)
	#clean_text = re.sub(r"\.\.\.+"," multi_point ",clean_text)
	#clean_text = re.sub(r"(\?!)+"," multi_surprise ",clean_text)
	
	#clean the h in the laugh
	clean_text = re.sub('hh+', "h", clean_text)
	clean_text = re.sub('aaa+', "a", clean_text)
	clean_text = re.sub(r"(ah){2,}|(ha){2,}"," laught ",clean_text)
	
	#remove the vowels repeated in sequence
	clean_text = re.sub("[a]{3,}","aa",clean_text)
	clean_text = re.sub("[e]{3,}","ee",clean_text)
	clean_text = re.sub("[i]{3,}","ii",clean_text)
	clean_text = re.sub("[o]{3,}","oo",clean_text)
	clean_text = re.sub("[u]{3,}","uu",clean_text)


	return clean_text
    

    
def preprocess(text,mode):
    preprocessed = text.copy()
    if mode == 'part':
        for i in range(len(text)):
            
            preprocessed[i] = re.sub(re.compile(r'\$[A-Z]{2,}'), ' ',preprocessed[i])
            #Step1: lowercase and remove stop words (a|an|the), remove reserved words (rt|fav|via)
            preprocessed[i] = ' '.join([word.lower() for word in preprocessed[i].split(' ') if word.lower() not in ['a','an','the','rt','fav','via']])

            #Step2:replace all urls with a tag "linktag"
            preprocessed[i] = re.sub(re.compile(r'(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&€+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)'), ' ',preprocessed[i])
            
            #Step3:replace all Hashtag (#...) with 'hashtag'
            preprocessed[i] = re.sub(re.compile(r'#\w*'), ' ',preprocessed[i])
        
            #Step4:replace all user mentions (@...) with 'usertag'
            preprocessed[i] = re.sub(re.compile(r'@\w*'), ' ',preprocessed[i])
        
            #Step5:replace all emoji and smiley icons with 'posemo'/'negemo'/'emoicon'
            preprocessed[i] = smiley_cleaner(preprocessed[i])
            preprocessed[i] = emoji_cleaner(preprocessed[i])
          
            #Step6:replace all Casgtags ($...) with 'cashtag'
            preprocessed[i] = re.sub(re.compile(r'\$[a-z]{2,}'), ' ',preprocessed[i])
        
            #Step7:replace all slang 
            #preprocessed[i] = slang_cleaner(preprocessed[i])
        
            #Step8:clean extra vowels, eg. 'cooooool'
            #preprocessed[i] = vv_cleaner(preprocessed[i])
        
            #Step9:clean badwords
            #preprocessed[i] = badword_cleaner(preprocessed[i])
        
            #Step10:replace all numbers (including dollars, percents e.g. $100, 4.5%) with 'numtag'
            preprocessed[i] = re.sub(re.compile(r'(-\$?\d+(?:.(\d+))?%?k?m?\$?)'), ' negnum ',preprocessed[i])
            preprocessed[i] = re.sub(re.compile(r'(\$?\d+(?:.(\d+))?%?k?m?\$?)'), ' posnum ',preprocessed[i])
                
            #Step11:deal with negation, add 'negtag_'prefix to all words following “not”, “no”, “none”, “nei- ther”, “never”or “nobody”.
            preprocessed[i] = neg_cleaner(preprocessed[i])
            
            #Step12:remove the punctuation except “! ? + – = ”
            preprocessed[i] =re.sub(r"(\(|\)|\[|\]|;|:|\.|,|\{|\}|\*|\||\<|\>|&|\/|@|#|\$|£|€|\^|~|\"|\'|_|\\|`|’)","",preprocessed[i])
	       
            #Step13:remove the single letters
            #preprocessed[i] =re.sub(r"\b\w\b"," ",preprocessed[i])
            
            #Step14:clean the extra whitespace
            t=preprocessed[i]
            while '  ' in t or ' \n' in t or '\n ' in t:
                t=t.replace('  ',' ').replace(' \n',' ').replace('\n ',' ')
            preprocessed[i] =t
            
    elif mode == 'clean':
        for i in range(len(text)):
            
            preprocessed[i] = re.sub(re.compile(r'\$[A-Z]{2,}'), ' ',preprocessed[i])
            #Step1: lowercase and remove stop words (a|an|the), remove reserved words (rt|fav|via)
            preprocessed[i] = ' '.join([word.lower() for word in preprocessed[i].split(' ') if word.lower() not in ['a','an','the','rt','fav','via']])

            #Step2:replace all urls with a tag "linktag"
            preprocessed[i] = re.sub(re.compile(r'(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&€+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)'), ' ',preprocessed[i])
            
            #Step3:replace all Hashtag (#...) with 'hashtag'
            preprocessed[i] = re.sub(re.compile(r'#\w*'), ' ',preprocessed[i])
        
            #Step4:replace all user mentions (@...) with 'usertag'
            preprocessed[i] = re.sub(re.compile(r'@\w*'), ' ',preprocessed[i])
        
            #Step5:replace all emoji and smiley icons with 'posemo'/'negemo'/'emoicon'
            preprocessed[i] = smiley_cleaner(preprocessed[i])
            preprocessed[i] = emoji_cleaner(preprocessed[i])
          
            #Step6:replace all Casgtags ($...) with 'cashtag'
            preprocessed[i] = re.sub(re.compile(r'\$[a-z]{2,}'), ' ',preprocessed[i])
        
            #Step7:replace all slang 
            preprocessed[i] = slang_cleaner(preprocessed[i])
        
            #Step8:clean extra vowels, eg. 'cooooool'
            preprocessed[i] = vv_cleaner(preprocessed[i])
        
            #Step9:clean badwords
            preprocessed[i] = badword_cleaner(preprocessed[i])
        
            #Step10:replace all numbers (including dollars, percents e.g. $100, 4.5%) with 'numtag'
            preprocessed[i] = re.sub(re.compile(r'(-\$?\d+(?:.(\d+))?%?k?m?\$?)'), ' negnum ',preprocessed[i])
            preprocessed[i] = re.sub(re.compile(r'(\$?\d+(?:.(\d+))?%?k?m?\$?)'), ' posnum ',preprocessed[i])
                
            #Step11:deal with negation, add 'negtag_'prefix to all words following “not”, “no”, “none”, “nei- ther”, “never”or “nobody”.
            preprocessed[i] = neg_cleaner(preprocessed[i])
            
            #Step12:remove the punctuation except “! ? + – = ”
            preprocessed[i] =re.sub(r"(\(|\)|\[|\]|;|:|\.|,|\{|\}|\*|\||\<|\>|&|\/|@|#|\$|£|€|\^|~|\"|\'|_|\\|`|’)","",preprocessed[i])
	       
            #Step13:remove the single letters
            preprocessed[i] =re.sub(r"\b\w\b"," ",preprocessed[i])
            
            #Step14:clean the extra whitespace
            t=preprocessed[i]
            while '  ' in t or ' \n' in t or '\n ' in t:
                t=t.replace('  ',' ').replace(' \n',' ').replace('\n ',' ')
            preprocessed[i] =t
            
                	
	
    return preprocessed

